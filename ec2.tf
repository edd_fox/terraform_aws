resource "aws_instance" "db-mysql" {
  ami                    = var.ami # viene de las variables
  availability_zone      = "us-east-2a"
  instance_type          = "t2.micro"
  key_name               = var.aws_key_name                 # viene de las variables
  vpc_security_group_ids = [aws_security_group.db.id]       # asociamos el nombre del SG de arriba
  subnet_id              = aws_subnet.us-east-2a-private.id # viene del vpc subnet privada
  private_ip             = "10.0.30.10"
  source_dest_check      = false # eso no lo recuerdo

  tags = {
    Name = "DB Server MySQL"
    Env  = "dev"
  }
}

resource "aws_instance" "db-oracle" {
  ami                    = var.ami
  availability_zone      = "us-east-2a"
  instance_type          = "t2.micro"
  key_name               = var.aws_key_name
  vpc_security_group_ids = [aws_security_group.db.id]
  subnet_id              = aws_subnet.us-east-2a-private.id
  #associate_public_ip_address = true
  private_ip        = "10.0.30.11"
  source_dest_check = false

  tags = {
    Name = "DB Server Oracle"
  }
}

resource "aws_instance" "web-1" {
  ami                         = var.ami # viene de las variables
  availability_zone           = "us-east-2a"
  instance_type               = "t2.micro"
  key_name                    = var.aws_key_name                # viene de las variables
  vpc_security_group_ids      = [aws_security_group.web.id]     # asociamos el nombre del SG de arriba
  subnet_id                   = aws_subnet.us-east-2a-public.id # viene del vpc subnet privada
  associate_public_ip_address = true
  source_dest_check           = false
  user_data                   = file("userdata1.sh")

  tags = {
    Name = "WEB server 1"
    Env  = "dev"
  }
}

resource "aws_eip" "web-1" {
  instance = aws_instance.web-1.id # lo asociamos con la instance
  vpc      = true
}

resource "aws_instance" "nat" {
  ami                         = var.ami_nat # viene de las variables
  availability_zone           = "us-east-2a"
  instance_type               = "t2.micro"
  key_name                    = var.aws_key_name
  vpc_security_group_ids      = [aws_security_group.nat.id] # hace referencia al SG creado arriba
  subnet_id                   = aws_subnet.us-east-2a-public.id
  associate_public_ip_address = true
  source_dest_check           = false

  tags = {
    Name = "VPC NAT"
  }
}

resource "aws_eip" "nat" {
  instance = aws_instance.nat.id
  vpc      = true
}

resource "aws_s3_bucket" "s3-bucket-edd" {
  bucket = var.bucket_name
  #acl = "private"

  tags = {
    Name        = "My Test Bucket"
    Environment = "VPC_EndPoint_Test"
  }
}