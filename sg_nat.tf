resource "aws_security_group" "nat" {
  name   = "vpc_nat"
  vpc_id = aws_vpc.main_vpc.id # nos traemos el id del vpc 

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "icmp"
    cidr_blocks = [var.private_subnet_cidr] # sólo a través de ésta red y viene de variables
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.private_subnet_cidr] # sólo a través de ésta red y viene de variables
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # desde afuera
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"] # desde afuera
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # desde afuera
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # desde afuera
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr] # viene de variables y solamente desde la vpc
  }

  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
