variable "region" {
  default = "us-east-2"
}

variable "ami" {
  default = "ami-002068ed284fb165b" #ami Amazon 5.10 Ohio
}

variable "ami_nat" {
  default = "ami-00d1f8201864cc10c" #ami NAT Amazon Ohio
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  default = "10.0.20.0/24"
}

variable "private_subnet_cidr" {
  default = "10.0.30.0/24"
}

variable "aws_key_name" {} # lo ponemos así porque usaremos tfvars

variable "aws_key_path" {} # lo ponemos así porque usaremos tfvars

variable "bucket_name" {
  default = "buckets3edd"
}
