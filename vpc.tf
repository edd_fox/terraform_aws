resource "aws_vpc" "main_vpc" {
  cidr_block           = var.vpc_cidr # nos traemos el valor de la varible vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "terraform_aws_vpc"
  }
}

resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.main_vpc.id # nos traemos el id del main_vpc de arriba

  tags = {
    Name = "Development IG"
  }
}

# Public Subnet
resource "aws_subnet" "us-east-2a-public" {
  vpc_id            = aws_vpc.main_vpc.id    # nos traemos el id del main_vpc de arriba
  cidr_block        = var.public_subnet_cidr # nos traemos el valor de la variable public_subnet_cidr
  availability_zone = "us-east-2a"

  tags = {
    Name = "Public Subnet"
  }
}

resource "aws_route_table" "us-east-2a-public" {
  vpc_id = aws_vpc.main_vpc.id # nos traemos el id del main_vpc de arriba
  route {
    cidr_block = "0.0.0.0/0"                # permitimos todo el segmento
    gateway_id = aws_internet_gateway.ig.id # asociamos con el internet gateway de arriba 
  }

  tags = {
    Name = "Route Table Public"
  }
}

resource "aws_route_table_association" "us-east-2a-public" {
  subnet_id      = aws_subnet.us-east-2a-public.id      # asociamos la subnet con el route table association
  route_table_id = aws_route_table.us-east-2a-public.id # asociamos el route table con el route table association
}

# Private Subnet
resource "aws_subnet" "us-east-2a-private" {
  vpc_id            = aws_vpc.main_vpc.id     # nos traemos el id del main_vpc de arriba
  cidr_block        = var.private_subnet_cidr # nos traemos el valor de la variable private_subnet_cidr
  availability_zone = "us-east-2a"

  tags = {
    Name = "Private Subnet"
  }
}

resource "aws_route_table" "us-east-2a-private" {
  vpc_id = aws_vpc.main_vpc.id # nos traemos el id del main_vpc de arriba
  route {
    cidr_block = "0.0.0.0/0"                # permitimos todo el segmento
    gateway_id = aws_internet_gateway.ig.id # asociamos con el internet gateway de arriba 
  }

  tags = {
    Name = "Route Table Private"
  }
}

resource "aws_route_table_association" "us-east-2a-private" {
  subnet_id      = aws_subnet.us-east-2a-private.id      # asociamos la subnet con el route table association
  route_table_id = aws_route_table.us-east-2a-private.id # asociamos el route table con el route table association
}

resource "aws_vpc_endpoint" "eps3" {
  vpc_id       = aws_vpc.main_vpc.id # nos traemos el id del main_vpc de arriba
  service_name = "com.amazonaws.us-east-2.s3"

}

resource "aws_vpc_endpoint_route_table_association" "edrts3" {
  route_table_id  = aws_subnet.us-east-2a-private.id # asociamos la subnet con el route table association
  vpc_endpoint_id = aws_vpc_endpoint.eps3.id
}